package com.pligor.mycrypto.crypto

import com.pligor.mycrypto.protocol.Writes

/**
 * Created with IntelliJ IDEA.
 * Developer: pligor
 */
protected trait Encryption {
  def encrypt(dataBytes: Array[Byte], secret: String): Array[Byte];
  def decrypt(codeBytes: Array[Byte], secret: String): Array[Byte];

  def encrypt[T:Writes](data: T, secret: String): Array[Byte] = {
    encrypt(implicitly[Writes[T]].writes(data), secret);
  }

  def encryptToString(dataBytes: Array[Byte], secret: String): String;

  def encryptToString[T:Writes](data: T, secret: String): String = {
    encryptToString(implicitly[Writes[T]].writes(data), secret);
  }
}
