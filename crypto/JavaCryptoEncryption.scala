package com.pligor.mycrypto.crypto

import javax.crypto.spec.SecretKeySpec
import javax.crypto.Cipher
import android.util.Base64
import scala.io.Codec

//import org.apache.commons.codec.binary.Base64

/**
 * Created with IntelliJ IDEA.
 * Developer: pligor
 */
protected class JavaCryptoEncryption(val algorithmName: String, val secretLen: Int) extends Encryption {
  private val codec = Codec.UTF8.charSet

  private val BASE64_FLAGS = Base64.NO_WRAP

  def encrypt(bytes: Array[Byte], secret: String): Array[Byte] = {
    val secretKey = new SecretKeySpec(secret.getBytes(codec), algorithmName)
    val encipher = Cipher.getInstance(algorithmName /*+ "/ECB/PKCS5Padding"*/)
    encipher.init(Cipher.ENCRYPT_MODE, secretKey)
    encipher.doFinal(bytes)
  }

  def decrypt(bytes: Array[Byte], secret: String): Array[Byte] = {
    val secretKey = new SecretKeySpec(secret.getBytes(codec), algorithmName)
    val encipher = Cipher.getInstance(algorithmName /*+ "/ECB/PKCS5Padding"*/)
    encipher.init(Cipher.DECRYPT_MODE, secretKey)
    encipher.doFinal(bytes)
  }

  def encryptToString(bytes: Array[Byte], secret: String): String = {
    //Base64.encodeBase64String(encrypt(bytes, secret));
    Base64.encodeToString(encrypt(bytes, secret), BASE64_FLAGS)
  }

  def decryptFromString(base64encoded: String, secret: String): String = {
    //new String(decrypt(Base64.decodeBase64(base64encoded), secret), codec);
    new String(decrypt(Base64.decode(base64encoded, BASE64_FLAGS), secret), codec)
  }
}
