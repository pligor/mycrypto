package com.pligor.mycrypto.crypto

/**
 * Created with IntelliJ IDEA.
 * Developer: pligor
 */
object AES extends JavaCryptoEncryption("AES", secretLen = 16);
