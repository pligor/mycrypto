package com.pligor.mycrypto.crypto

/**
 * Created with IntelliJ IDEA.
 * Developer: pligor
 */
object DES extends JavaCryptoEncryption("DES", secretLen = 8);
