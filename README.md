Helper classes to encrypt and decrypt strings. It is meant for encryption and decryption of small objects.

Consider using openpgp in order to encode large objects as files or very long strings.

### Dependencies ###
None

=========================

Used in projects:

* [bman](http://bman.co)
* [Fcarrier](http://facebook.com/FcarrierApp)