package com.pligor.mycrypto.protocol

import scala.io.Codec

/**
 * Created with IntelliJ IDEA.
 * Developer: pligor
 */
object defaults {

  implicit object WritesString extends Writes[String] {
    def writes(value: String) = value.getBytes(Codec.UTF8.charSet)
  }

  implicit object WritesLong extends DataOutputStreamWrites[Long](_.writeLong(_))

  implicit object WritesInt extends DataOutputStreamWrites[Int](_.writeInt(_))

  implicit object WritesShort extends DataOutputStreamWrites[Short](_.writeShort(_))

}
