package com.pligor.mycrypto.protocol

import scala.annotation.implicitNotFound

/**
 * Created with IntelliJ IDEA.
 * Developer: pligor
 */
@implicitNotFound(msg = "Could not find a Writes for ${T}")
trait Writes[T] {
  def writes(value: T): Array[Byte];
}
